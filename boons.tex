\chapter[House of Potions \& Boons]{Potions \& Boons}

\section{Magical Boons}

\begin{multicols}{2}

\subsection{Situations}

Sometimes the environment is just conducive to a certain type of magic.
Situations only favour the high spheres.

\subsubsection{Force}

\begin{itemize}
  \item
  Falling (cast fast!)
  \item
  New moon above.
\end{itemize}

\subsubsection{Death}

\begin{itemize}
  \item
  Unmoving air (must be underground, and no movement or breathing for a day).
\end{itemize}

\subsubsection{Life}

\begin{itemize}
  \item
  During an earthquake.
  \item
  As a cat gives birth.
\end{itemize}

\subsubsection{Light}

\begin{itemize}
  \item
  During an eclipse.
  \item
  Staring into full-body, up-close reflections (those created by spells don't count).
\end{itemize}

\subsubsection{Mind}

\begin{itemize}
  \item
  Dreams about dreams.
\end{itemize}

\subsection{Ingredients}
\label{magicIngredients}
\index{Ingredients}
\index{Concoction}

Certain ingredients have magical properties, and with the right preparation can become a powerful \gls{boon} which boosts any caster's abilities.
The caster throws the liquid or powder in the air, to use as a catalyst for a spell.

Anyone around the affected area can use up the ambient power -- whoever casts fastest receives the benefits.
Of course, casters typically make use of these items when standing at a fair distance from any enemy.

\begin{itemize}
  \item
  Concoctions add +1 \gls{mp} to a spell which employs that element.
  \item
  The caster counts as having +1 in the appropriate elemental sphere, even if they have a rating of 0 without the concoction.
  \item
  Casters can only employ one concoction for a given sphere -- anything more does nothing but waste the ingredients.
\end{itemize}

Casters can employ concoctions to cast high-sphere spells they could not cast without.
For example, a mage with Earth 1, Water 2 could only cast Life spells with a cost of 1; but with enough dwarf-beard, properly prepared, they could cast a level-2 spell.

\subsubsection{Air}

\begin{itemize}
  \item
  Basilisk gullet.
  \item
  Griffin feathers.
  \item
  The last air of a drowning victim, sealed in a jar.
  \item
  Ashes of a full dragon wing.
  \item
  Gnoll teeth.
\end{itemize}

\subsubsection{Fate}

\begin{itemize}
  \item
  Dragon eggs.
  \item
  The web-spinning organ of a chitincrawler.
  \item
  Embalmed finger of a monarch.
  \item
  Five-toed rabbit's foot.
  \item
  Human blood.
\end{itemize}

\subsubsection{Earth}

\begin{itemize}
  \item
  Basilisk hide.
  \item
  3 kilograms of Dwarven beard.
\end{itemize}

\subsubsection{Fire}

\begin{itemize}
  \item
  A woodspy's eye.
  \item
  Ashes of a gnome.
  \item
  Dragon's heart.
\end{itemize}

\subsubsection{Water}

\begin{itemize}
  \item
  Elven tears.
  \item
  Frozen frogspawn (select species only).
  \item
  Large oyster pearls.
\end{itemize}

\end{multicols}

\section{Talisman Workshop}

\begin{multicols}{2}

\noindent
With the right ingredients, one can create one-use items (`talismans'), to store a single spell.

\subsubsection{Creating Talismans}

\begin{itemize}
  \item
  A talisman requires double the \glspl{mp} that the spell would require.
  \item
  At least half the \glspl{mp} for the talisman must come from magical ingredients.
  \item
  Talismans must be made from the right ingredients for the elemental spheres involved.
  \begin{description}
    \item[Air]
    uses contained gasses, often held in a phial.
    \item[Earth]
    requires any solid materials dug from the ground.
    \item[Fate]
    talismans require the remains of someone who had \glspl{fp} in life (at least 1 per \gls{mp} cost of the spell).
    \item[Fire]
    requires flammable material.
    \item[Water]
    can use any liquid.
  \end{description}
  Spells which employ multiple spheres must employ multiple ingredients.
  \item
  The creator then decides upon a talisman's activation conditions (see below).
  \item
  The alchemical process requires an \roll{Intelligence}{Alchemy} check, \tn[7] plus the spell's cost.
  \item
  Spells are cast with a bonus equal to their \gls{mp} cost, so an Air spell with a cost of 3 \glspl{mp}, made into a talisman, casts with a straight +3 bonus, rather than a roll of \roll{Charisma}{Air}.
\end{itemize}

\subsubsection{Talisman Activation}

During the processes, the alchemist emphasises the activation, over and over.
It might be a word, phrase, a concept, condition, or type of tree.
So some items activate when they see the light, others wait until the stars above match the stars when their creator made them.

The creator must impress a talisman's `waking conditions' upon it so well that the ritual always leaves a `telling scar'.
If air in a phial must activate when the right stars show, then the creator will likely carve the pattern of those stars into the phial.
If a scroll, containing a spell of Fire, should activate upon hearing the word `warden', then that word should be written, or heavily implied by writings upon the scroll.

Sometimes talismans `decide' to activate at just the right time, displaying a surprising understanding of context.
At other times, they seem to have a ridiculously literal interpretation of their own activation.
In any case, they have no minds of their own -- merely an echo of their creator's singular thought at the time of the ritual which made them.

\subsubsection{Identifying Talismans}

A \textit{Detailed Witness} spell, employing all of the correct spheres, can ask about whether the contained spell employs particular Enhancements or Effects.
The \gls{tn} for this effect is 7 plus the spell's \gls{mp}.

Of course, this only identifies the \emph{effects} of a talisman, not the activation.

\end{multicols}

\section{Enchantments \& Creations}
\label{magicalMinds}

\begin{multicols}{2}

\subsection{Ageless Minds}
When enchanters grow the minds of little creatures, the results vary.
Some become clever, others remain fairly dim.
The dim creations often serve best, as they have very little chance of rebelling.
Mind-mages who try to create more intelligent spells often find their creations develop their own ideas before long, and soon after start making their own decisions.

Every created mind has its own quirks, but they broadly fall under the following categories, determined by the spell's level.

\mapentry{Basic Minds}

When minds arise from such basic spells, they only want to do one thing, and cannot understand nuance or detail.
It cannot even understand conditionals -- it does one thing, always, and thinks of nothing else.

\begin{itemize}
  \item
    `Kill jester'
  \item
    `Defend me'
  \item
    `Go North'
\end{itemize}

These minds have a Wits total of 0, and no other Mental Attributes (just like animals).
Casters might use these spells to have illusions act on their own, or create a door-knocker which curses all humans it sees.

\mapentry{Rising Minds}

Basic minds act like well-trained dogs.
They understand two-part orders, or conditions, and will carry these out with some basic discernment.
Spell casters favour these basic minds, as they remain obedient for a long time.

\begin{itemize}
  \item
    `Find John and whisper this message to him'
  \item
    `Approach the king, then make all the fires around explode'
  \item
    `Go to the village, then report back to me'
\end{itemize}

While useful, these items eventually gain little `quirks', as they find some tasks more enjoyable than others.
Mostly, their desires spawn from the caster's more primitive desires, which creates a dark parody of the caster.

\begin{exampletext}
The witch who wants to dress in fine clothes and create sentient, acidic creatures, will find her new creature intentionally hunts down find clothes, then dissolves them slowly before it hunts for more.

The seeker-sorcerer who feels curious about everything and summons sentient illusions will soon see them crowd around him as they try to interrogate him and see how he responds to their movements.

Jesters who summon sentience from the inanimate can come to nothing but a messy, and hilarious end.
Their basic creations will not know empathy or perspective, or even self-preservation.
They only seek a punch-line.

\end{exampletext}

These minds have a Wits Bonus equal to $1D6-3$.

\mapentry{Sentient Mind}

Intelligent spells exhibit strange behaviour.
They can think and reason, but have no humanoid values.
They don't want food or mates (usually), and have never had a job.
They rarely show much sense of self-preservation, and may decide to end their own existence out of simple boredom.

In short, spells are not well socialized.
However, they understand any instruction their creator person would.

When the caster creates such a mind, it obeys its orders enthusiastically, like a child with only one toy.
If the caster needs the spell to protect everyone from goblins, it will protect.
If no goblins remain alive, it will try to find some goblins to attack the caster so that it can continue `protecting'.
If it cannot find any goblins, it will argue the definition of `goblin'.

These minds have Intelligence, Wits, and Charisma, each equal to $1D6-3$.

\begin{itemize}
  \item
    `Kill any elves who look like they're up to something'
  \item
    `Can you stop killing them now?
    I said \emph{stop}!'
  \item
    `Why would I treat you like an equal?
    You're just a thing I made!'
\end{itemize}

\mapentry{Genius}

Clever spells are almost always a mistake.
They begin play with some of the caster's memories, and often have magical spheres.

If the caster asks the new mind to draw up plans for a fortress, things may go well at first.
But within a week, the new sentience may start demanding better building materials, then threaten to blackmail the caster with all the secrets it knows about them.
By the time the caster realises something has gone wrong, the new creature may have gained a few nearby allies and taken to ordering other sentient magical creations about.
Some even attempt to impersonate the real caster, claiming to have been magically imprisoned, while an imposter walks around, looking like them.

\mapentry{Demonic Army}

Top-level enchantment spells, as ever, affect many targets.
This is never not a recipe for disaster.

A full brigade of sentient rats, each with a united plan, is no laughing matter for anyone in the area.
In fact the new army of rats may well be added to the local encounter table.
If they survive well, they could mate, multiply, and become a new race.

\subsection{Bound Spirits}

Ghouls -- hungry spirits, trapped in a corpse -- make for deadly servants.
Mages with Death magic have no particular ability to control them, so in practice, all `necromancers' also employ Mind magic.
Without it, they only have the power to summon monsters, but not to control them.

\end{multicols}
