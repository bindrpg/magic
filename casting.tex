\chapter{Spell Weaving}

Magic does not exactly create.
It warps, waxes, and wanes.
It gives information.
But nothing in the world comes from nothing, so spellcasters have to work with the environment, but they can only change what their spheres govern.

Casters create a spell's foundation by deciding what \emph{action} to perform on which element.
Fire casters may \textit{Wax Fire}, causing it to roar and explode.
Water casters can \textit{Wane the River}, making it evaporate.
\textit{Witness Fate} spells allow the caster to know if fate has plans for someone nearby.
So each of the five spheres allows the caster to create four basic spells -- wax, wane, warp, and witness.

More advanced casters can add enhancements, though they come with complications and costs.
The `duplicated' enhancement lets spells affect a wider area, or more targets, but the caster may inadvertently target allies.
`Distant' spells can affect anyone far away, but not someone close by.
Each enhancement added increases the spell's potency, at the potential cost of unwanted complications.

On top of the basic five low spheres, casters can employ high spheres by combining two of the low spheres together.
Air and Fate create Death magic, while Water and Earth create Life magic.

\section{The Rules}

\begin{multicols}{2}

\noindent
Spell casters speak to the elements by rolling Charisma plus some Sphere (Earth, Air, Mind, et c.).
The \gls{tn} is always 7, adjusted for the target's resistance.
The resistance from a rock, or the wind depends on the state of the target (making wind more windy is easy, but summoning a hurricane underground is not).
The resistance to targeting a person depends on the person; they may resist in any number of ways.

People can resist Mind spells by focussing their mind, or resist necromancy by tensing their muscles.
And all spells might be resisted by simply stabbing the spellcaster.

\vspace{1em}
\begin{exampletext}
  An archer draws back his bow, intent on hitting the witch. At the same time, she speaks a curse, hoping to make him fumble, and miss his shot, while depleting his \glspl{fp}.
  The \gls{tn} is 7 plus the archer's \roll{Dexterity}{Projectiles}.
\end{exampletext}

\begin{exampletext}
  A water-mage attempts to Warp Water, freezing the water surrounding everyone in the flood. Bandits slosh through the flooded city, attempting to stab at him. The \gls{tn} is 7 plus the bandits' \roll{Dexterity}{Combat}.
\end{exampletext}

\begin{exampletext}
  The enchanter wants to control the elf's mind. He can't fight well, but his mind is strong. Instead of attacking the caster, he guards his mind, and resists with \roll{Wits}{Vigilance}.
\end{exampletext}

\subsubsection{Casting Range}
More powerful spells fizzle after shorter distances.
A basic spell has a range of 20 steps, and every level increase decreases the range by 5 steps.

\sidebox[3]{
  \begin{boxtable}[YL]
    \textbf{Level} & \textbf{Range} \\
    \hline
    1 & 15 steps \\
    2 & 10 steps \\
    3 & 5 steps \\
    4 & touch \\
  \end{boxtable}
}

A level 4 spell demands the mage touch the first target.
The other targets must also be touching each other, allowing the spell to flow through them like lightning.

\subsubsection{Magical Ingredients}

Various special items in the world can enhance a caster's abilities.
Almost all of them require a lot of preparation to turn into a fine dust which can permeate the air around the spell.
Anyone can throw the dust in the air to aid spell-casting.

Griffin feathers, ground to dust, grant a bonus to Air magic.
Dragon eggs, once split open, can enhance any caster's Fate Skill.

Casters, or non-casters, do not need a minimum level in any sphere to use these enhancements.
Someone with no magical abilities can use these items.
However, the use of items can only grant plus \emph{one} to any sphere, never more.

For more on these ingredients, see \autopageref{magicIngredients}.

\subsection{Enhancements}

Enhancements bolster some aspect of a spell, while adding to a spell's level.
Duplicated spell affects more targets, depending on the spell's level, and Detailed spells add subtlety, which also increase the spell's level.

Each Enhancement added increases the effects of the other dramatically, but can also bring the burden of an unwanted side effect.
Someone with the ability to cast a fourth-level spell may want to create a heavy blast of wind to throw a troublemaker at the tavern back through the door he came through, but they \emph{must} gain three enhancements to raise the spell to the fourth level. 
If they make the spell Duplicated, everyone in the tavern will feel the blast; if they make it Divergent, they must select some other element, which will create yet another spell; if they make it Distant then the wind's force will begin far away, at the horizon and do nothing to anyone next to the caster.
Most casters make a spell Detailed when looking for a safe Enhancement to add, but even then, unexpected problems often arise.

At their peak, all spheres of magic display the same problems: the spell will target only the farthest distances, will affect many targets (even if the caster doesn't want them to), will always display some peculiar appearance (which usually identifies the caster), and will create some other spell with the same action (`Wax', `Wane', et c.), but new targets.
Casters who want to give someone magical wings cannot make wings -- they will end up bestowing wings on someone while they stand upon a distant mountain-peak, and all the earth in the area becomes as brittle as glass.

\subsubsection{Detailed}

Detailed spells let the caster select how the spell appears.
Instead of making a bonfire simply explode, the caster can make it explode while looking like a mythical bird, or ice can melt into water while taking on the form of humans writing in pain.
The caster can also make spells more particular about targets, so that fire spell attacks only enemies, or a Mind spell might make someone forget what they wanted to do about that one things\ldots{} what was it again?

Detailed spells always betray something of the caster -- perhaps the writing on an illusion looks like their handwriting, or maybe the winds they summon always looks like their screaming face.
A successful \roll{Wits}{Empathy} roll at \tn[7], plus the spell's level, indicates that anyone who knows the caster can identify their hand at play.

\subsubsection{Divergent}

Casters can add secondary effects to spells by adding another sphere.
The second spell's effects are identical, except for the additional sphere.

For example:

\begin{itemize}
  \item
  A `Wax Earth' spell hardens the bog around an enemy's feet.
  By adding `Air', the spell gains the same effects as `Wax Air', and the complete spell becomes `Divergent Wax Air and Earth'.
  \item
  A doula hopes to break a cheeky customer's mind with a `Detailed Wane Mind` spell, which makes them forget about what they came into the shop for.
  With a crooked smile, she decides to make it also `divergent', with the `Life' sphere.
  In total, the spell will cost 3 \glspl{mp}, and will send the customer out with no memory of how they lost the ability to move their toes.
\end{itemize}

\subsubsection{Distant}

Standard spells reach out only a few steps, but \textit{distant} spells can reach across worlds.
They also give spells a \emph{minimum} distance; casting a spell at `throwing distance' means the spell must fly beyond the standard maximum of a spell's distance (15 steps).
Similarly, spells with a range of `shouting distance' \emph{must} find a target beyond throwing distance.

\sidebox[3]{
  \begin{boxtable}[Yl]
    \textbf{Level} & \textbf{Range} \\
    \hline
    2 & Throwing distance \\
    3 & Shouting distance \\
    4 & Horizon \\
    5 & Line of Sight \\
  \end{boxtable}
}

If the caster misjudges the range while weaving a spell, the spell will find some other target inside the proper range.

Spells targeting the \gls{ainumar} result in the caster's instant death from a responding spell.
Nobody knows why, but they assume a god or gods are responsible.

\subsubsection{Duplicated}

Most spells have a single target, but by making a spell \textit{duplicated}, the spell goes up to level 2, and affects double the usual targets.
Earth spells might affect double the usual \gls{weight}, Mind spells affect two minds instead of one, and duplicated Fire spells can target a multitude of nearby torches, candles, and anything else currently alight.
If the caster can employ another Enhancement, then the spell multiplies \emph{again} by 3, and then by 4, and so on.

\sidebox[2]{
  \begin{boxtable}[YY]
    \textbf{Level} & \textbf{Targets} \\
    \hline
    2 & 2 \\
    3 & 6 \\
    4 & 24 \\
    5 & 120 \\
  \end{boxtable}
}

With enough Enhancements pushing a spell's level up, spells can affect huge numbers of targets.
The incredible force of a caster with a strong stock of magical items has serious implications for warfare across \gls{fenestra}.
While armies can fall prey to a single enchanter's song, a small, strong-willed group of veterans can push through and succeed.

\end{multicols}

\section{Spheres of Magic}
\index{Spheres of Magic}

\begin{multicols}{2}

\subsection{Elemental Spheres}
\index{Elemental Spheres}

\Gls{fenestra} has no need for philosophers to debate the fundamentals of the world.
Spellcasters can alter five basic building-blocks -- Air, Earth, Fate, Fire, and Water -- so they have first-hand knowledge of the world's essential nature.

\subsubsection{Modes}
\index{Modes}
Each sphere a character gains gives them access to four modes of casting -- four ways to affect the element.

\begin{description}
  \item[Waxing]
  spells encourage the element, making it \emph{more} like what it is.
  \item[Waning]
  magic does the opposite -- it reduces the element's nature, and often destroys the target.
  \item[Warping]
  an element alters some fundamental aspect, promoting strange behaviour and effects from the target.
  \item[Witnessing]
  means to find out whether or not the element exists.
  These spells let the caster know if the element lies nearby.
\end{description}

\subsubsection{Air}
\hint{wind, smoke, steam, fog, mist, cloud}

\begin{description}
  \item[Wax]
  spells increase the air's motion, making even a still room fast enough to push someone back.
  \item[Wane]
  spells make air putrid enough to make people choke and retch.
  Breathing the smoke in inflicts 1 \gls{fatigue} per level of the spell.
  \item[Warp]
  spells can reshape air into a more solid `bubble'.
  \item[Witness]
  air spells don't come up very often, but the spell remains, nevertheless.
\end{description}

\subsubsection{Earth}
\hint{dirt, ice, snow, sand, ash}

\begin{description}
  \item[Wax]
  spells harden sand into stone, or bind snow into ice.
  \item[Wane]
  softens earth, melts ice, turns dirt to mud, and cracks rocks.
  \item[Warp]
  spells twist earth in unnatural ways, making it brittle.
  These spells can increase the \gls{tn} to damage a wall or door, but if they receive a single point of Damage, the brittle mass may shatter.
\end{description}

\subsubsection{Fate}
\hint{luck, curses, prophecy}

\begin{description}
  \item[Wax]
  fate spells grant 2 \glspl{fp} plus the spell's level.
  The \glspl{fp} convert to dice, just like Damage, so a level 1 \textit{Wax Fate} spell would grant $1D6-1$ \glspl{fp}.
  \item[Wane]
  spells inflict curses, removing the same number of \glspl{fp}.
  \item[Warp]
  fate spells inflict \emph{encounters} on a target.
  Each level adds one encounter per interval.%
  \iftoggle{judgement}{%
    \footnote{\Glspl{gm} looking for encounter ideas can find a system in \textit{Judgement}: \nameref{encounters}, \autopageref{encounters}.}%
  }{}
  \item[Witness]
  spells tell a caster if someone has \glspl{fp}.
  Witches once informed people when they had found `the chosen one', but so many fated for great things end up eaten by the forest that nowadays nobody puts much stock in someone with an aura of luck.
\end{description}

\subsubsection{Fire}
\hint{flame, lightning, heat, furnaces}

\begin{description}
  \item[Wax]
  spells make fires flare up and burn through their fuel in an explosive instance.
  \item[Wane]
  spells put fires out.
  Miracle workers who truly understand fire can often do more damage through darkness than damage.
  \item[Warp]
  spells can remove a fire's light, make them flicker downwards rather than up, or alter the colours.
\end{description}

\subsubsection{Water}
\hint{rivers, rain, ale, magma}

\begin{description}
  \item[Wax]
  spells make rivers flow faster, rain fall harder and purify any liquid, from ales to straight-up poisons.
  \item[Wane]
  encourages water's natural evaporation.
  Small-scale spells might encourage a cup of water to evaporate, while greater magics could turn a river into steam.
  \item[Warp]
  spells freeze liquids, or encourage any mildly impure substance to become putrid, or even acidic.
\end{description}

\subsection{High Spheres}

High spheres combine two of the lower, elemental spheres to produce something new.
If someone can cast both of the lower spheres they need, then they can also cast from that high sphere at a level equal to the lowest of the constituents.

\begin{exampletext}
  For example, a miracle worker with Water 2, and Fate 1 can automatically cast spells from the Mind sphere at level 1.
  If they gain the Earth sphere at level 2, they could then combine Water and Earth magic to make Life magic, also at level 2.
\end{exampletext}

\subsubsection{Death}
\hint{rot, health, souls, fatigue, regeneration}

\textbf{Elements:}
\roll{Air}{Fate}

\begin{description}
  \item[Wax]
  spells increase any deathly effects, inflicting \glspl{fatigue} equal to the spell's level.
    \begin{description}
      \item[Detailed]
        versions can remove \glspl{hp} instead of adding \glspl{fatigue}.
    \end{description}
  \item[Wane]
    spells remove degradation.
    The target ignores a number penalties from \glspl{fatigue} equal to the spell's level.

    These spells always cause problems in the long-run.
    Each interval the target engages in any physical activity whatsoever, the spell weakens a little, and after a number of \glspl{interval} equal to the spell's level, it ends, crushing the target with \gls{fatigue} penalties.
    \begin{description}
      \item[Detailed]
        spells focus this slowing of \glspl{fatigue} onto a single problem, so the target ignores \gls{fatigue} penalties gained through hiking, poison, fighting, or any other source, equal to double the spell's level.
        For example, a level 3 spell would allow the target to ignore up to a -6 \gls{fatigue} penalty.
      \item[Objects]
        targeted by this spell rot more slowly.
        \footnote{Some use the spell to preserve food, though it always loses its flavour.
        This has resulted in the nickname `death rations' for any food affected by the spell.}
      \item[Undead corpses,]
        animated by angry spirits, cannot move properly in their dead shell.
        This spell allows a corpse to become supple enough to move, although the body continues to rot whenever the creature moves.%
        \footnote{For more on the duration of death magic, see \autopageref{deathDuration}.}
        This leaves mindless undead falling apart after they spend long enough searching for souls, but sentient undead always feel aware of this slow degradation and guard their ability to sleep viciously.
        Many spend centuries standing perfectly still, fearing any situation which might make them move, and bring them closer to the afterlife which still awaits them.
    \end{description}
  \item[Warp]
    spells pull pull souls between worlds, detaching the living from life, and pulling the dead into the living world.
    \begin{description}
      \item[People]
        affected by these spells enter a kind of limbo between life and death.
        Their heart slows, but they keep moving just as fast.
        People so affected bleed less, and gain an unnatural \gls{dr} of 2, just like the undead.

        The target also becomes detached from the world around them, and begin to treat the world like a kind of strategy game.
        The uncaring mindset, glazed eyes, and strange conversation reduces their Charisma bonus by a number equal to the spell's level.
        \begin{description}
          \item[Detailed]
            spells can twist a human's soul until they begin to see as the dead see.%
            \exRef{judgement}{Judgement}{undead}
        \end{description}
      \item[Spirits]
        are not easily targeted by these spells, as the caster must have the ability to view and interac with what they want to cast upon.
        Therefore these spells must begin with a `Witness Death' spell, to request information about the dead souls in the area.
        But once the mage has a target, they can bind that target to a fresh corpse, greating a `ghoul'.

        A failed roll may indicate spell failure, or that the caster has bound the wrong type of spirit, and now has a very intelligent undead creature on their hands.
        These creatures can `play dumb' like nothing else, and wait for the perfect opportunity to stab the caster in the back.
        \begin{description}
          \item[Detailed]
            spells can draw spirits into less suitable bodies, such as skulls, doorways, statues, or dead animals.
            If the vessel is not a body, then the spirit will not be able to move.
            However, spirits with Death magic can still cast a Witness spell to perceive the living world and then cast further spells.
        \end{description}
    \end{description}
  \item[Witness]
    spells track dead spirits who somehow cling to this realm, evading their proper afterlife.%
    \exRef{judgement}{Judgement}{godsOfDeath}
    These spells allow mages to target spirits with other spells, but only a moment before the spirit moves.
    \begin{description}
      \item[Detailed]
        Witness spells allow the caster to receive more specific information, such as the location of a relative who passed away, and then (if that spell succeeds) to ask if that spirit wants to express something about a particular subject.

        As usual, only `yes' or `no' answers return.
        The caster cannot simply ask a spirit to divulge information, because they cannot hear the spirit -- they can only confirm or disconfirm guesses about what the spirit might want to say.
    \end{description}
\end{description}

\subsubsection{Force}
\hint{momentum, portals, teleportation}

\textbf{Elements:}
\roll{Fire}{Earth}

\begin{description}
  \item[Wax]
  force means a heavy force, often used in combat.
  These spells let anything deal Damage equal the spell's $level + 3$, but the spell must be cast at \emph{exactly} the same time as the target weapon hits something.
  In mechanical terms, the caster must release the spell when they have exactly the same number of \glspl{ap} as the attack they want to aid.

  \begin{exampletext}
    An alchemist has Fire 2 and Earth 1, which implies access to the Force sphere at level 1.
    He wants to aid his companion's arrows, with a \textit{Wax Force} spell.
    Unfortunately they already deal $1D6+3$ Damage, 
  \end{exampletext}
  \item[Wane]
  force means to remove momentum or gravity.
  The target may lose a number of \glspl{ap} equal to the spell's level, or gain a bonus to jumping equal to to the spell's level +2.%
  \footnote{The target's \gls{weight} also reduces by 1 per spell level.}
  While people break free from the spell's effects through their own motions, inanimate objects do not.
  A sword stripped of its own weight will simply float around indefinitely.
  Likewise, someone who becomes near weightless can simply relax, and float along with the wind indefinitely.
  Unfortunately for them, they have little say in which direction they will float, so flight is not on the cards without some very clever Air magic prepared beforehand.
  \item[Warp]
  forces crate tears in space\ldots or possibly just one tear in space.
  This spell creates a magical `rift' which joins one place to another, allowing anyone to step through one side, and step out the other.
  This spell may initially appear to have two targets (the entrance, and exit), but since both places become one, the spell has only one target -- one `rift'.
  These rifts have two symmetric sides, so a portal placed over a kitchen's door might lead to the garden outside, and the other side of that door would lead to the same place in the garden, but out the other side of the rift.

  These portals have delicate edges, so any damage to them will destroy them.
  Creatures moving through them must make a \roll{Dexterity}{Larceny} check, with a \gls{tn} of 7 plus their total \gls{weight} (including all items they carry).%
  \footnote{Of course, anyone is free to throw their items through the portal first, and (hopefully) follow after them.}
  Building any kind of wall around the portal to protect the border makes the opening smaller, and ensures the maximum \gls{weight} which can move through it, equals the spell's level plus 6.

  As usual, a \textit{duplicated} spell allows for a larger \gls{weight} in total (as it effectively increases the spell's level).
  Spells of level 4 or more will allow creatures with up to a \gls{weight} of 24, which should allow just about anything through without any bother.
  \item[Witness]
  of force allows the caster to know where any heavy forces act (flying arrows, heavy falling objects, or magical rifts).
\end{description}

\subsubsection{Life}
\hint{flesh, metabolism, plants, bone}

\textbf{Elements:}
\roll{Earth}{Water}

\begin{description}
  \item[Wax]
  spells cause growth.
    \begin{description}
      \item[Creatures]
        made to grow increase in muscle, height, and overall mass, which always comes out a little misshapen and strange -- the results always appear monstrous, or at least unsettling.
        Unfortunately, the large size also makes them clumsy.

        \begin{itemize}
          \item
          The target's Strength increases, until it equals the spell's level plus half the target's current Strength.
          If this does not increase the target's Strength, the spell stops here, with no effect.
          \item
          Charisma decreases by a number equal to the spell's level, as their mind becomes polluted with bestial thoughts and their features contort.
          This penalty affects \glspl{fp} as usual.
          \item
          Dexterity also suffers a penalty equal to the spell's level, but this subsides after the target grows accustomed to the new form.
          Each day spent practising with the new body allows a \roll{Wits}{Athletics} roll to reduce the penalty by 1.
        \end{itemize}
      \item[Detailed]
        spells allow more control over the changes in body; they do not make anyone clumsy, but instead \emph{fast}.
        Unfortunately, the persistently twitching, pulsating, body impacts the target's ability to think straight.

        This functions as above, but boosts Speed instead of Strength, and decreases only Intelligence.
        This Intelligence penalty does not subside.
      \item[Plants]
        which grow large steal the nutrients from the soil around them.
        After a couple of spells, the ground nearby will no longer grow any edible plants.
        Continued use of this magic will always manage to grow \emph{something}, but that something will neither be strong, nor pretty (and definitely not edible).

        Plants which grow through magic can do so very fast -- easily at ten times the normal rate.
        Of course this may still require a few days before any notable effect.
        The caster does not need to wait for the plant to grow, as the spell will continue working once cast successfully.
      \item[Detailed]
        spells which target plants can grow far faster, and in just the direction the mage desires.
        This allows casters to summon a thorny trap around sleeping targets, cross a narrow river with vegetation grasping at each other from each side, or warp a tree into a make-shift hut within a scene (assuming the spell targets a large enough area).
    \end{description}
  \item[Wane]
  spells make life decay, in one way or another.
    \begin{description}
      \item[Creatures]
        might distort, causing them to falter when running, or grow large feet, or muscles may decay.
        Powerful spells can even shrink targets somewhat.

        The caster can select any Physical Attribute, and give it a penalty equal to the spell's level.
      \item[Plants]
        afflicted by any such spells become sickly, and eventually die.
        A standard vegetable will resist with \tn[4], while wild plants such as bushes and reeds resist at \tn[7].
        Large, hardy trees can put up more of a fight, easily reaching \glspl{tn} 12, 14, or more.
      \item[Detailed]
        attacks allow the caster to specify more precise appearances to the deformity, giving the target exceptionally small eyes, or making them look hairy and bestial, or simply remove their mouth.

        The mechanical effects might reduce the target's ability to see or hear, or to speak; or even decrease how many \glspl{fatigue} they can regenerate while resting by sealing up their mouth (which makes eating rather difficult).
        However this affects a target, the penalty is equal to the spell's level.
    \end{description}
  \item[Warp]
    spells produce fully new effects in creatures.
    Larger changes require higher level-spells, while smaller changes require lower-level spells; so the caster must get the level bang-on to produce the right effects.
    \begin{enumerate}
      \item
        Antlers allow someone to inflict +\arabic{enumi} Damage when brawling toe-to-toe.
      \item
        Teeth allow the target to grapple and deal Damage in a single action, but remove any ability to speak.
      \item
        Thick skin affords +\arabic{enumi} \gls{dr}, but makes movement tiring, earning them +\arabic{enumi}~\glspl{fatigue}.
      \item
        Quadrupedal movement, like a wolf or horse, allows the target to move twice as fast as they otherwise could, but removes their ability to grasp anything.
      \item
        Wings allow a target to fly, as long as their \roll{Speed}{Air} equals or exceeds Strength.
        Heavier targets can still glide long distances, but will have to climb somewhere high before they do so.
    \end{enumerate}
    \begin{description}
      \item[Detailed]
        spells let casters create complex, and even internal mutations.
        \begin{enumerate}
            \stepcounter{enumi}
          \item
            \setcounter{track}{\value{enumi}}%
            \addtocounter{track}{\value{track}}%
            Venom sacks allow any successful grapple to inject a nasty liquid which inflicts \arabic{track}~\glspl{fatigue}.
          \item
            Tentacles permit the character a +\arabic{enumi} bonus to brawl actions.
          \item
            A nauseous, gaseous, stench spills from the target.
            They can always breathe their own gases, but others will suffer \arabic{enumi}~\glspl{fatigue} when staying in their presence too long, at least as long as the wind does not clear the air.
          \item
            Magical abilities can appear with the highest levels.
            Creatures may destroy any flames nearby, or cause plants to grow.
            The magical effect will always work in a similar way, as it comes by instinct rather than with any consideration for the result.
            The effect always uses the Wits Attribute, costs one \gls{ap}, and count as a level 1 Skill.%
            \footnote{This natural Skill remains separate from any of the creature's actual Skills, and never adds to their magical Skills.}
      \item[Plants]
        can become poisonous, hardy, or begin growing prolifically.
        As ever, most effects create some mechanical effect with a number equal to the spell's level.
        \end{enumerate}
  \item[Witness]
    spells tell the mage if any living thing lies nearby.
    These spells typically suffer from having far too much life all around.
    No forest has a single point which has no life.
    \begin{description}
      \item[Detailed]
        spells allow the caster to ask more information, such as whether something animal, or humanoid exists in the chosen direction.
        Higher levels could even identify someone from a particular family, or (at their zenith) an individual.
    \end{description}
    \end{description}
\end{description}

\subsubsection{Light}
\hint{reflections, illusions, shadow, astronomy}

\textbf{Elements:}
\roll{Air}{Fire}

\begin{description}
  \item[Wax]
    spells let a candle-flame light up a feasting hall, and make Sunlight blinding.
    Even the feint light of stars can blossoms enough to let travellers see when walking at night.

    Anyone afflicted with by the light resists with \roll{Wits}{Vigilance} or takes a penalty equal to the spell's level to any actions involving sight.
    \begin{description}
      \item[Detailed]
        light spells often take on a specific form, such as a candle which appears like a glittering diamond, or splitting Sunbeams into rainbow patterns.
        No matter what form the light takes, it still shines brightly.
    \end{description}
  \item[Wane]
    spells reduce light to a feint glow\ldots~and then a shadow.

    The target can sneak with a bonus equal to the spell's level, at least when travelling in a dark area.
    However, casting a `cloak of shadows' over oneself in the middle of a Sunlit field will only draw people's attention.
    \begin{description}
      \item[Detailed]
        shadow spells can become silhouettes, which can fool onlookers into thinking they represent real people, creatures, or even structures.
    \end{description}
  \item[Warp]
    spells can twist light into illusions -- phantasmic things, made of nothing but their appearance, just a form without substance.
    Any harsh movements against them (such as an attack in combat) instantly disrupt the light, and scattering the phantom into motes of shining dust before it vanishes.
    Most of these illusions appear thoroughly unrealistic, so nobody will be fooled in good lighting.
    \begin{description}
      \item[Detailed]
        illusions seem very convincing, even up-close, and can produce a little sound while the caster focusses.

        Onlookers can resist being taken in by the lie by resisting with \roll{Wits}{Vigilance}.
    \end{description}
  \item[Witness]
    A detect light spell doesn't sound the greatest ability in the world\ldots because it isn't.
    However, a crafty witch can make use of even the least popular spells.
\end{description}

\subsubsection{Mind}
\hint{reflections, illusions, shadow, astronomy}

\textbf{Elements:}
\roll{Fate}{Water}

\begin{description}
  \item[Wax]
    spells blossom a thought into a fully-blown sentience.
    People with a wild thought become obsessed with it, as they become seduced by the clarity of their desires, and the ease of thought when thinking about their one, narrow, thought.
    Despite the spell's force, targets usually fail to notice that anything strange has happened to them.
    `Work, work', or `that was rude', the target thinks, and the thought blooms until the target can think about little else.
    The spell does not make anyone think something new, it merely enforces their drive and focus.

    The target gains a bonus to whichever Mind Attribute they were using at the time the spell was cast%
    \footnote{If the target's `currently used Mind Attribute' seems unclear, the \gls{gm} is encouraged to make something up and explain it post-facto, but then they hardly need reminded of that.}

    The spell blossoms that Mind Attribute, adding its level.
    The total Attribute Bonus equals whichever was highest (the original Mind Attribute, or the spell's level), plus half of the lower.%
    \exRef{core}{Core Rules}{stacking}
    The target enjoys the use of this heightened Mind Attribute only under the very limited circumstances of fulfilling the thought which birthed it.

    Many a spellcaster has had the `bright idea' to study magic all night by using this spell on themself, and inevitably becomes wrapped up in repeatedly casting a single spell on a particular target, again and again, or simply rereading a single paragraph until the Sun rises, and they collapse from exhaustion.
  \item[Animals]
    targeted by this spell behave quite differently.
    Since they have little in the way of their own minds, this blossoming thought can grow until it takes over the creature entirely.
  \item[Spells]
    begin as an extension of the caster's own mind, so they have some measure of sentience.
    This means that any spell can develop `a mind of its own', with the right encouragement.
    \begin{description}
      \item[Detailed]
        spells allow the caster to select which of the target's thoughts bloom.
        The caster must select something which seems like an obvious plan or desire, or they will target a non-existent thought, and the spell will fail.
        When cast on a person, this results in a demonic voice in their head, beckoning them to do this or that.

        This new mind gains 1 Skill level per level of the spell.
        This may allow a person to fight as if possessed by the caster (including a bonus to Combat, if the caster has sufficient Skill to improve the target), or even to gain levels in arcane Skills.
    \end{description}
    For more on making a mind, and how the new mind acts, see \autopageref{magicalMinds}.

  \item[Wane]
    The target struggles to think clearly, and suffers a penalty to all Mind Attributes equal to the spell's level.
    \begin{description}
      \item[Detailed]
        waning spells allow the caster to wrench particular memories from someone.
        If the spell succeeds, these memories will \emph{never} return.
    \end{description}
  \item[Warp]
    spells switch one Code for another, giving the target a very different value system.
    The change never presents in an obvious or immediate fashion -- after all, someone who has spent their life saving lives, or stealing money, believes that they value these things.
    The first inklings of the alteration occur slowly, as the target finds little excuses for their new desires.
    \begin{description}
      \item[Detailed]
        spells twist the fundamentals of how the mind operates, rewiring one Skill to another.
        Rewriting Combat to Empathy will prompt targets to reason with people instead of stabbing them, while rewiring Empathy to Combat means the target attempts to understand people and show affection by sparring with the object of their attentions.

        Each level of the spell allows the caster to rewrite a different Skill to another Skill, which must be used in its place.
        Any time the target uses an afflicted Skill, they must either swap this Skill for its alternative, or take a penalty equal to the spell's level.

        Players should not know exactly which of their Skills has been affected, although it will be very obvious if they use the Combat Skill to assault an enchanter, who then resists with this spell.
    \end{description}
  \item[Witness]
    spells allow the enchanter to know if any thoughts lie nearby.
    A simple `yes', might indicate a nearby sparrow has spotted a worm, so the spell will not always give useful information.
    \begin{description}
      \item[Detailed]
        spells to detect minds can inquire about particular thoughts.
        `Is there violence here?'
        `Does anyone seek gold?'
    \end{description}
\end{description}

\subsection{Spell Duration \& Banishment}

All spells last forever, or perhaps they vanish instantly but leave the world changed.

\begin{description}
  \item[Air]
  dissipates eventually, although moving air remains in motion, and choking fogs remain noxious.
  \item[Earth]
  which breaks remains broken, and ice statues melt in the Sun.
  \item[Fate]
  runs its course.
  \item[Fire]
  can burn forever, so some rich houses keep an enchanted fire going, burning in all of the colours of the rainbow.
  \item[Water]
  always returns to its natural form in time, once mist settles, or acid mixes with normal water, and joins the natural cycles of the sea and rivers.
  \item[Death]
  \begin{itemize}
    \item
    spells which simply debilitate heal normally.
    \item
    Warping effects reduce by one as the target embraces life, and starts to push themselves.
    At the end of any \gls{interval} in which the target endures \pgls{fatigue} penalty, the spell disappears.
    \item
    Waning effects, which reduce the rate of death, dissipate with movement.
    Each \gls{interval} in which the target engages in exercise, they reduce any effects by a single point.
    \label{deathDuration}
  \end{itemize}
  \item[Force]
  \begin{itemize}
    \item
    effects cast on people dissipate as the person moves.
    Every \gls{ap} spent reduces the effects by 1.
    \item
    Warping effects (portals) vanish once the sides are touched, as the rift in space collapses.
  \end{itemize}
  \item[Light]
  spells suffer from any type of push or movement -- even a strong wind will eventually crack an illusion.
  \item[Life]
  \begin{itemize}
    \item
    Life enhancing magic remains fuelled by the body of the affected (or `afflicted').
    These spells end only through starvation or tiredness, once the \gls{fatigue} penalty reaches -4.
    \item
    Warping spells work the same, although the impatient will often simply cut the wings off.
    \item
    Life spells which `Wane' will leave the afflicted until they heal.
    People will heal at the same rate as they heal \glspl{hp}, and only heal once all \glspl{hp} have healed.
  \end{itemize}
  \item[Mind]
  altering effects depart as quickly as a habit, meaning that they might linger for some time.
  Whatever the effects, when someone gains \pgls{xp} for following their code, they can shake off the spell.%
  \footnote{Spending money does not count.}
\end{description}

\end{multicols}

