This repository will slowly become the rules for magic.

Broadly, it should work like this:

1. Players receive simple spells on their sheet.
    * Spells show something like this:
    * **Party-Kill: Fire 2, Area: Room, Range: 5 steps.**
    * *Snuff out all the fire in a room*
2. All spells should be created with an underlying spell-system.
    * Players who want to read all the rules should have a very clear idea of how to create spells on the fly.
3. Various items will count as one-use mana-tokens.
    * Griffins feathers (properly prepared) might grant 1 Air.
    * A woodspy's heart could grant 1 Earth.
    * Items will soil, and go bad, quickly.
4. I have no idea how magical items are going to function, but the rules need to have fewer steps than the current system.

[Download WiP Version][download]

# Changes

## Attribute Emphasis

- Standard magic will use Charisma as the base stat. Spellcasters will coax plants, by speaking to them sweetly, or demand a fire banish itself.
- A Fast-Casting Knack will be available to use Wits to cast even faster spells, allowing uncharismatic casters to exist.
- A Ritual-Casting Knack knack will allow casters to use Intelligence, and to gain larger amounts of Mana.

[download]: https://gitlab.com/bindrpg/magic/-/jobs/artifacts/master/raw/magic.pdf?job=build
